<?php

namespace App\Models\Daas\AcessaTG;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EmpenhosDaas extends Model
{

    public function buscaEmpenhosD1UnidadeAno(string $unidade, string $documento_ano, string $data_d1)
    {
        $sql = "select
            DISTINCT
            max(a19.CO_UG)  ug,
            max(a18.CO_GESTAO)  gestao,
            right(a11.ID_DOCUMENTO, 12)  numero,
            max(a17.NO_DIA_COMPLETO) emissao,
            a16.ID_TP_ENTIDADE  tipo_favorecido,
            a11.ID_ENTIDADE_FAVO_DOC  favorecido,
            max(a16.NO_ENTIDADE)  nome_favorecido,
            a11.TX_OBSERVACAO  observacao,
            max(a110.CO_FONTE_RECURSO_DETA)  fonte,
            max(a113.CO_NATUREZA_DESPESA)  naturezadespesa,
            a15.ID_PI  planointerno,
            max(a111.NO_PI)  nome_planointerno,
            a14.ID_LISTA_ITENS_NE  num_lista
            from	DWTG_Colunar_VBL.WD_DOCUMENTO	a11
            join	DWTG_Colunar_VBL.WD_UG_EXERCICIO	a12
            on 	(a11.ID_ANO_EMISSAO_DOC = a12.ID_ANO and
            a11.ID_UG = a12.ID_UG)
            join	DWTG_Colunar_VBL.WD_ORGAO	a13
            on 	(a12.ID_ORGAO_UG = a13.ID_ORGAO)
            join	DWTG_Colunar_VBL.WD_DOC_NE	a14
            on 	(a11.ID_DOC_NE = a14.ID_DOC_NE)
            join	DWTG_Colunar_VBL.WF_DOC_NE	a15
            on 	(a11.ID_DOC_NE = a15.ID_DOCUMENTO)
            join	DWTG_Colunar_VBL.WD_ENTIDADE	a16
            on 	(a11.ID_ENTIDADE_FAVO_DOC = a16.ID_ENTIDADE and
            a11.ID_TP_ENTIDADE_FAVO_DOC = a16.ID_TP_ENTIDADE)
            join	DWTG_Colunar_VBL.WD_DIA	a17
            on 	(a11.ID_ANO_EMISSAO_DOC = a17.ID_ANO and
            a11.ID_DIA_EMISSAO_DOC = a17.ID_DIA and
            a11.ID_MES_EMISSAO_DOC = a17.ID_MES)
            join	DWTG_Colunar_VBL.WD_GESTAO	a18
            on 	(a13.ID_GESTAO_PRIN = a18.ID_GESTAO)
            join	DWTG_Colunar_VBL.WD_UG	a19
            on 	(a12.ID_UG = a19.ID_UG)
            join	DWTG_Colunar_VBL.WD_FONTE_RECURSO_DETA	a110
            on 	(a15.ID_FONTE_RECURSO_DETA = a110.ID_FONTE_RECURSO_DETA)
            join	DWTG_Colunar_VBL.WD_PI	a111
            on 	(a15.ID_ORGAO_PI = a111.ID_ORGAO_PI and
            a15.ID_PI = a111.ID_PI)
            join	DWTG_Colunar_VBL.WD_TP_ENTIDADE	a112
            on 	(a16.ID_TP_ENTIDADE = a112.ID_TP_ENTIDADE)
            join	DWTG_Colunar_VBL.WD_NATUREZA_DESPESA	a113
            on 	(a15.ID_CATEGORIA_ECONOMICA_NADE = a113.ID_CATEGORIA_ECONOMICA_NADE and
            a15.ID_ELEMENTO_DESPESA_NADE = a113.ID_ELEMENTO_DESPESA_NADE and
            a15.ID_GRUPO_DESPESA_NADE = a113.ID_GRUPO_DESPESA_NADE and
            a15.ID_MOAP_NADE = a113.ID_MOAP_NADE)
            where	(a11.ID_TP_DOCUMENTO in ('NE')
            and a12.ID_UG='$unidade'
            and a11.ID_ANO_EMISSAO_DOC = '$documento_ano'
            and a11.DT_CARGA_C='$data_d1'
            and a14.ID_ESPECIE_NE in (1, 7))
            group by	a12.ID_UG,
            a13.ID_GESTAO_PRIN,
            a11.ID_DOCUMENTO,
            a14.ID_DOC_NE_REFE,
            a11.ID_ANO_EMISSAO_DOC,
            a11.ID_MES_EMISSAO_DOC,
            a11.ID_DIA_EMISSAO_DOC,
            a16.ID_TP_ENTIDADE,
            a11.ID_TP_ENTIDADE_FAVO_DOC,
            a11.ID_ENTIDADE_FAVO_DOC,
            a11.TX_OBSERVACAO,
            a15.ID_FONTE_RECURSO_DETA,
            a15.ID_CATEGORIA_ECONOMICA_NADE,
            a15.ID_GRUPO_DESPESA_NADE,
            a15.ID_MOAP_NADE,
            a15.ID_ELEMENTO_DESPESA_NADE,
            a15.ID_ORGAO_PI,
            a15.ID_PI,
            a14.ID_LISTA_ITENS_NE";

        return DB::connection('odbc-dwtg')
            ->select($sql);
    }


    public function buscaEmpenhosCargaUnidadeAno(string $unidade, string $documento_ano)
    {
        $sql = "select
            DISTINCT
            max(a19.CO_UG)  ug,
            max(a18.CO_GESTAO)  gestao,
            right(a11.ID_DOCUMENTO, 12)  numero,
            max(a17.NO_DIA_COMPLETO) emissao,
            a16.ID_TP_ENTIDADE  tipo_favorecido,
            a11.ID_ENTIDADE_FAVO_DOC  favorecido,
            max(a16.NO_ENTIDADE)  nome_favorecido,
            a11.TX_OBSERVACAO  observacao,
            max(a110.CO_FONTE_RECURSO_DETA)  fonte,
            max(a113.CO_NATUREZA_DESPESA)  naturezadespesa,
            a15.ID_PI  planointerno,
            max(a111.NO_PI)  nome_planointerno,
            a14.ID_LISTA_ITENS_NE  num_lista
            from	DWTG_Colunar_VBL.WD_DOCUMENTO	a11
            join	DWTG_Colunar_VBL.WD_UG_EXERCICIO	a12
            on 	(a11.ID_ANO_EMISSAO_DOC = a12.ID_ANO and
            a11.ID_UG = a12.ID_UG)
            join	DWTG_Colunar_VBL.WD_ORGAO	a13
            on 	(a12.ID_ORGAO_UG = a13.ID_ORGAO)
            join	DWTG_Colunar_VBL.WD_DOC_NE	a14
            on 	(a11.ID_DOC_NE = a14.ID_DOC_NE)
            join	DWTG_Colunar_VBL.WF_DOC_NE	a15
            on 	(a11.ID_DOC_NE = a15.ID_DOCUMENTO)
            join	DWTG_Colunar_VBL.WD_ENTIDADE	a16
            on 	(a11.ID_ENTIDADE_FAVO_DOC = a16.ID_ENTIDADE and
            a11.ID_TP_ENTIDADE_FAVO_DOC = a16.ID_TP_ENTIDADE)
            join	DWTG_Colunar_VBL.WD_DIA	a17
            on 	(a11.ID_ANO_EMISSAO_DOC = a17.ID_ANO and
            a11.ID_DIA_EMISSAO_DOC = a17.ID_DIA and
            a11.ID_MES_EMISSAO_DOC = a17.ID_MES)
            join	DWTG_Colunar_VBL.WD_GESTAO	a18
            on 	(a13.ID_GESTAO_PRIN = a18.ID_GESTAO)
            join	DWTG_Colunar_VBL.WD_UG	a19
            on 	(a12.ID_UG = a19.ID_UG)
            join	DWTG_Colunar_VBL.WD_FONTE_RECURSO_DETA	a110
            on 	(a15.ID_FONTE_RECURSO_DETA = a110.ID_FONTE_RECURSO_DETA)
            join	DWTG_Colunar_VBL.WD_PI	a111
            on 	(a15.ID_ORGAO_PI = a111.ID_ORGAO_PI and
            a15.ID_PI = a111.ID_PI)
            join	DWTG_Colunar_VBL.WD_TP_ENTIDADE	a112
            on 	(a16.ID_TP_ENTIDADE = a112.ID_TP_ENTIDADE)
            join	DWTG_Colunar_VBL.WD_NATUREZA_DESPESA	a113
            on 	(a15.ID_CATEGORIA_ECONOMICA_NADE = a113.ID_CATEGORIA_ECONOMICA_NADE and
            a15.ID_ELEMENTO_DESPESA_NADE = a113.ID_ELEMENTO_DESPESA_NADE and
            a15.ID_GRUPO_DESPESA_NADE = a113.ID_GRUPO_DESPESA_NADE and
            a15.ID_MOAP_NADE = a113.ID_MOAP_NADE)
            where	(a11.ID_TP_DOCUMENTO in ('NE')
            and a12.ID_UG='$unidade'
            and a11.ID_ANO_EMISSAO_DOC = '$documento_ano'
            and a14.ID_ESPECIE_NE in (1, 7))
            group by	a12.ID_UG,
            a13.ID_GESTAO_PRIN,
            a11.ID_DOCUMENTO,
            a14.ID_DOC_NE_REFE,
            a11.ID_ANO_EMISSAO_DOC,
            a11.ID_MES_EMISSAO_DOC,
            a11.ID_DIA_EMISSAO_DOC,
            a16.ID_TP_ENTIDADE,
            a11.ID_TP_ENTIDADE_FAVO_DOC,
            a11.ID_ENTIDADE_FAVO_DOC,
            a11.TX_OBSERVACAO,
            a15.ID_FONTE_RECURSO_DETA,
            a15.ID_CATEGORIA_ECONOMICA_NADE,
            a15.ID_GRUPO_DESPESA_NADE,
            a15.ID_MOAP_NADE,
            a15.ID_ELEMENTO_DESPESA_NADE,
            a15.ID_ORGAO_PI,
            a15.ID_PI,
            a14.ID_LISTA_ITENS_NE";

        return DB::connection('odbc-dwtg')
            ->select($sql);
    }


    public function buscaItensEmpenhosD1UnidadeAno(string $unidade, string $documento_ano, string $data_d1)
    {
        $sql = "select
                distinct
                a13.ID_LISTA_ITENS_NE as lista_item,
                a13.ID_SUBITEM_NADE  as subitem,
                a13.ID_ITEM  as numitem,
                a13.QT_ITEM  as quantidade,
                a13.VA_ITEM  as valorunitario,
                (a13.QT_ITEM * a13.VA_ITEM)  as valortotal,
                a12.TX_OBSERVACAO  as descricao,
                sum((a11.VA_MOVIMENTO_LIQUIDO * a15.PE_TAXA))  as saldo
                from	DWTG_Colunar_VBL.WF_LANCAMENTO	a11
                join	DWTG_Colunar_VBL.WD_DOCUMENTO	a12
                  on 	(a11.ID_ANO_LANC = a12.ID_ANO_EMISSAO_DOC and
                a11.ID_DOCUMENTO_LANC = a12.ID_DOCUMENTO)
                join	DWTG_Colunar_VBL.WD_DOC_NE_ITEM	a13
                  on 	(a12.ID_DOC_NE = a13.ID_DOC_NE)
                join	DWTG_Colunar_VBL.WD_MOEDA	a14
                  on 	(a11.ID_MOEDA_UG_EXEC_H = a14.ID_MOEDA)
                join	DWTG_Colunar_VBL.WD_TAXA_CAMBIO_MENSAL	a15
                  on 	(a14.ID_MOEDA = a15.ID_MOEDA_ORIGEM)
                join	DWTG_Colunar_VBL.WA_MES_ACUM_SEM_ANO	a16
                  on 	(a11.ID_MES_LANC = a16.ID_MES_ACUM_ANO_SALDO)
                join	DWTG_Colunar_VBL.WD_DOC_NE	a17
                  on 	(a12.ID_DOC_NE = a17.ID_DOC_NE)
                join	DWTG_Colunar_VBL.WD_UG_EXERCICIO	a18
                  on 	(a11.ID_ANO_LANC = a18.ID_ANO and
                a11.ID_UG_EXEC = a18.ID_UG)
                join	DWTG_Colunar_VBL.WD_ORGAO	a19
                  on 	(a18.ID_ORGAO_UG = a19.ID_ORGAO)
                join	DWTG_Colunar_VBL.WD_UG	a110
                  on 	(a18.ID_UG = a110.ID_UG)
                join	DWTG_Colunar_VBL.WD_NATUREZA_DESPESA_DETA	a111
                  on 	(a11.ID_CATEGORIA_ECONOMICA_NADE = a111.ID_CATEGORIA_ECONOMICA_NADE and
                a11.ID_ELEMENTO_DESPESA_NADE = a111.ID_ELEMENTO_DESPESA_NADE and
                a11.ID_GRUPO_DESPESA_NADE = a111.ID_GRUPO_DESPESA_NADE and
                a11.ID_MOAP_NADE = a111.ID_MOAP_NADE and
                a11.ID_SUBITEM_NADE = a111.ID_SUBITEM_NADE and
                a13.ID_CATEGORIA_ECONOMICA_NADE = a111.ID_CATEGORIA_ECONOMICA_NADE and
                a13.ID_ELEMENTO_DESPESA_NADE = a111.ID_ELEMENTO_DESPESA_NADE and
                a13.ID_GRUPO_DESPESA_NADE = a111.ID_GRUPO_DESPESA_NADE and
                a13.ID_MOAP_NADE = a111.ID_MOAP_NADE and
                a13.ID_SUBITEM_NADE = a111.ID_SUBITEM_NADE)
                join	DWTG_Colunar_VBL.WD_GESTAO	a112
                  on 	(a19.ID_GESTAO_PRIN = a112.ID_GESTAO)
                where	(a18.ID_UG = '$unidade'
                 and a11.ID_ANO_LANC = '$documento_ano'
                  and a11.DT_CARGA_C = '$data_d1'
                 and a11.ID_TP_DOCUMENTO in ('NE')
                 and a17.ID_ESPECIE_NE in (1, 7)
                 and a15.ID_ANO = a11.ID_ANO_LANC
                 and a15.ID_MES = a16.ID_MES)
                group by	a18.ID_UG,
                a19.ID_GESTAO_PRIN,
                a13.ID_ITEM,
                a13.ID_DOC_NE,
                a13.ID_LISTA_ITENS_NE,
                a13.ID_CATEGORIA_ECONOMICA_NADE,
                a13.ID_GRUPO_DESPESA_NADE,
                a13.ID_MOAP_NADE,
                a13.ID_ELEMENTO_DESPESA_NADE,
                a13.ID_SUBITEM_NADE,
                a13.VA_ITEM,
                a13.QT_ITEM,
                (a13.QT_ITEM * a13.VA_ITEM),
                a13.ID_LISTA_ITENS_NE,
                a12.TX_OBSERVACAO";

        return DB::connection('odbc-dwtg')
            ->select($sql);

    }

    public function buscaItensEmpenhosCargaUnidadeAno(string $unidade, string $documento_ano, string $meses)
    {
        $sql = "select
                distinct
                a13.ID_LISTA_ITENS_NE as lista_item,
                a13.ID_SUBITEM_NADE  as subitem,
                a13.ID_ITEM  as numitem,
                a13.QT_ITEM  as quantidade,
                a13.VA_ITEM  as valorunitario,
                (a13.QT_ITEM * a13.VA_ITEM)  as valortotal,
                a12.TX_OBSERVACAO  as descricao,
                sum((a11.VA_MOVIMENTO_LIQUIDO * a15.PE_TAXA))  as saldo
            from	DWTG_Colunar_VBL.WF_LANCAMENTO	a11
                join	DWTG_Colunar_VBL.WD_DOCUMENTO	a12
                  on 	(a11.ID_ANO_LANC = a12.ID_ANO_EMISSAO_DOC and
                a11.ID_DOCUMENTO_LANC = a12.ID_DOCUMENTO)
                join	DWTG_Colunar_VBL.WD_DOC_NE_ITEM	a13
                  on 	(a12.ID_DOC_NE = a13.ID_DOC_NE)
                join	DWTG_Colunar_VBL.WD_MOEDA	a14
                  on 	(a11.ID_MOEDA_UG_EXEC_H = a14.ID_MOEDA)
                join	DWTG_Colunar_VBL.WD_TAXA_CAMBIO_MENSAL	a15
                  on 	(a14.ID_MOEDA = a15.ID_MOEDA_ORIGEM)
                join	DWTG_Colunar_VBL.WA_MES_ACUM_SEM_ANO	a16
                  on 	(a11.ID_MES_LANC = a16.ID_MES_ACUM_ANO_SALDO)
                join	DWTG_Colunar_VBL.WD_DOC_NE	a17
                  on 	(a12.ID_DOC_NE = a17.ID_DOC_NE)
                join	DWTG_Colunar_VBL.WD_UG_EXERCICIO	a18
                  on 	(a11.ID_ANO_LANC = a18.ID_ANO and
                a11.ID_UG_EXEC = a18.ID_UG)
                join	DWTG_Colunar_VBL.WD_ORGAO	a19
                  on 	(a18.ID_ORGAO_UG = a19.ID_ORGAO)
                join	DWTG_Colunar_VBL.WD_UG	a110
                  on 	(a18.ID_UG = a110.ID_UG)
                join	DWTG_Colunar_VBL.WD_NATUREZA_DESPESA_DETA	a111
                  on 	(a11.ID_CATEGORIA_ECONOMICA_NADE = a111.ID_CATEGORIA_ECONOMICA_NADE and
                a11.ID_ELEMENTO_DESPESA_NADE = a111.ID_ELEMENTO_DESPESA_NADE and
                a11.ID_GRUPO_DESPESA_NADE = a111.ID_GRUPO_DESPESA_NADE and
                a11.ID_MOAP_NADE = a111.ID_MOAP_NADE and
                a11.ID_SUBITEM_NADE = a111.ID_SUBITEM_NADE and
                a13.ID_CATEGORIA_ECONOMICA_NADE = a111.ID_CATEGORIA_ECONOMICA_NADE and
                a13.ID_ELEMENTO_DESPESA_NADE = a111.ID_ELEMENTO_DESPESA_NADE and
                a13.ID_GRUPO_DESPESA_NADE = a111.ID_GRUPO_DESPESA_NADE and
                a13.ID_MOAP_NADE = a111.ID_MOAP_NADE and
                a13.ID_SUBITEM_NADE = a111.ID_SUBITEM_NADE)
                join	DWTG_Colunar_VBL.WD_GESTAO	a112
                  on 	(a19.ID_GESTAO_PRIN = a112.ID_GESTAO)
            where	(a18.ID_UG = '$unidade'
             and a11.ID_MES_LANC in ($meses)
             and a11.ID_ANO_LANC = '$documento_ano'
             and a11.ID_TP_DOCUMENTO in ('NE')
             and a17.ID_ESPECIE_NE in (1, 7)
             and a15.ID_ANO = a11.ID_ANO_LANC
             and a15.ID_MES = a16.ID_MES)
            group by	a18.ID_UG,
                a19.ID_GESTAO_PRIN,
                a13.ID_ITEM,
                a13.ID_DOC_NE,
                a13.ID_LISTA_ITENS_NE,
                a13.ID_CATEGORIA_ECONOMICA_NADE,
                a13.ID_GRUPO_DESPESA_NADE,
                a13.ID_MOAP_NADE,
                a13.ID_ELEMENTO_DESPESA_NADE,
                a13.ID_SUBITEM_NADE,
                a13.VA_ITEM,
                a13.QT_ITEM,
                (a13.QT_ITEM * a13.VA_ITEM),
                a13.ID_LISTA_ITENS_NE,
                a12.TX_OBSERVACAO";

        return DB::connection('odbc-dwtg')
            ->select($sql);

    }

}
