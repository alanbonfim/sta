<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empenho extends Model
{
    protected $table = 'empenhos';

    protected $fillable = [
        'ug',
        'gestao',
        'numero',
        'numero_ref',
        'emissao',
        'tipofavorecido',
        'favorecido',
        'observacao',
        'fonte',
        'naturezadespesa',
        'planointerno',
        'numlista',
        'rp'
    ];

    public function buscaEmpenho()
    {
        $sql = 'SELECT ';
        $sql .= '';
        $sql .= '';
        $sql .= '';
        $sql .= '';
        $sql .= '';
        $sql .= '';
        $sql .= 'FROM empenhos ';
    }

    function itens() {
        return $this->hasMany(Empenhodetalhado::class, 'numeroli', 'num_lista');
    }

}
