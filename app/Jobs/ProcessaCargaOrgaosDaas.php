<?php

namespace App\Jobs;

use App\Http\Controllers\Api\v1\AcessaTGDaasController;
use App\Http\Controllers\Api\v1\ContratosOrgaosController;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessaCargaOrgaosDaas implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $timeout = 900;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $processo = new AcessaTGDaasController();
//        $processo->orgaos();
        $processo->execJdbcClass('orgaos');
    }
}
