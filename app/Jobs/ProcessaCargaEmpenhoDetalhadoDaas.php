<?php

namespace App\Jobs;

use App\Http\Controllers\Api\v1\AcessaTGDaasController;
use App\Models\Orgao;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessaCargaEmpenhoDetalhadoDaas implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $timeout = 900;
    protected $unidade;
    protected $ano;
    protected $meses;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $unidade, string $ano, string $meses)
    {
        $this->unidade = $unidade;
        $this->ano = $ano;
        $this->meses = $meses;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $processo = new AcessaTGDaasController();
//        $processo->itensEmpenhosCargaUnidadeAno($this->unidade, $this->ano, $this->meses);
        $processo->execJdbcClass('cargaitensempenhos', $this->unidade, $this->ano, $this->meses);
    }
}
