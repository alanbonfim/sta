<?php

namespace App\Http\Controllers\Api;

use App\Models\Naturezasubitens;
use App\Models\Orgao;
use App\Models\Unidade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EstruturaController extends Controller
{
    public function buscaOrgaosSuperiores()
    {
        $orgaos_superiores = Orgao::where('orgao_superior','')
            ->orderBy('codigo')
            ->get();

        return json_encode($orgaos_superiores);
    }

    public function buscaOrgaos()
    {
        $orgaos = new Orgao();

        return json_encode($orgaos->buscaTodosOrgaos());
    }

    public function buscaUnidades()
    {
        $unidades = new Unidade();

        return json_encode($unidades->buscaUnidade());

    }

    public function buscaNaturezadespesas()
    {
        $naturezadespesa = new Naturezasubitens();

        return json_encode($naturezadespesa->buscaNaturezaDespesas());

    }
}
