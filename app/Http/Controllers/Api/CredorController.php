<?php

namespace App\Http\Controllers\Api;

use App\Models\Credor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CredorController extends BaseController
{
    public function ler()
    {

        $nomearquivo = $this->buscaArquivos();

        $pkCount = (is_array($nomearquivo) ? count($nomearquivo) : 0);
        if ($pkCount != 0) {
            foreach ($nomearquivo as $nome) {
                if (substr($nome['nomearquivo'], 0, 6) == 'credor' or substr($nome['nomearquivo'], 6, 6) == 'credor') {
                    $credor = $this->lerArquivo($nome['nomearquivo'], $nome['case']);

                    foreach ($credor as $e) {
                        $busca = $this->buscaCredor($e['cpf_cnpj_idgener']);
                        if (!isset($busca->cpf_cnpj_idgener)) {
                            $novo_listaempenho = new Credor;
                            $novo_listaempenho->cpf_cnpj_idgener = $e['cpf_cnpj_idgener'];
                            $novo_listaempenho->tipofornecedor = $e['tipo_fornecedor'];
                            $novo_listaempenho->nome = $e['nome'];
                            $novo_listaempenho->uf = $e['uf'];
                            $novo_listaempenho->save();
                        }else{
                            if($busca->nome != $e['nome']){
                                $busca->nome = $e['nome'];
                                $busca->save();
                            }
                        }
                    }
                }
            }

            $ok = 'Credores lidos.';
        } else {
            $ok = 'Não Há arquivos para leitura.';
        }

        return $ok;

    }

    public function buscaCredor($cnpjcpfidgener)
    {

        $credor = Credor::where('cpf_cnpj_idgener', $cnpjcpfidgener)
            ->first();


        return $credor;

    }

    public function lerArquivo($nomeaquivo, $case)
    {
        $path = config('app.path_pendentes');
        $path_processados = config('app.path_processados');
        $name = $path . $nomeaquivo;
        $namedestino = $path_processados . $nomeaquivo;

        if($case == 0){
            $extref = ".REF.gz";
            $exttxt = ".TXT.gz";
        }

        if($case == 1){
            $extref = ".ref.gz";
            $exttxt = ".txt.gz";
        }

        $myfileref = gzopen($name . $extref, "r") or die("Unable to open file!");

        $i = 0;
        while (!gzeof($myfileref)) {
            $line = gzgets($myfileref);

            if (strlen($line) == 0) break;

            $ref[$i]['column'] = trim(substr($line, 0, 40));
            $ref[$i]['type'] = trim(substr($line, 40, 1));

            if (strstr(trim(substr($line, 42, 4)), ",") != FALSE) {
                $num = explode(",", trim(substr($line, 42, 4)));
                $ref[$i]['size'] = $num[0] + $num[1];
                $ref[$i]['decimal'] = $num[1];
            } else {
                $ref[$i]['size'] = trim(substr($line, 42, 4)) * 1;
                $ref[$i]['decimal'] = 0;
            }

            $ref[$i]['acu'] = ($i == 0) ? $ref[$i]['size'] : $ref[$i]['size'] + $ref[$i - 1]['acu'];
            $i++;
        }
        $NUMCOLS = $i;
        gzclose($myfileref);

        $myfiletxt = gzopen($name . $exttxt, "r") or die("Unable to open file!");
        $i = 0;
        $j = 0;
        while (!gzeof($myfiletxt)) {
            $line = gzgets($myfiletxt);
            for ($j = 0; $j < $NUMCOLS; $j++) {
                $campo = $ref[$j]['column'];
                $inicio = ($j == 0) ? 0 : $ref[$j - 1]['acu'];
                $valor = trim(substr($line, $inicio, $ref[$j]['size']));
                if ($ref[$j]['type'] == "N") {
                    $valor = $valor * pow(10, -$ref[$j]['decimal']);
                }
                if ($campo == 'IT-CO-CREDOR') {
                    $credor[$i]['dado'] = $valor;
                }
                if ($campo == 'IT-CO-TIPO-CREDOR') {
                    $credor[$i]['tipo_fornecedor'] = $valor;
                }
                if ($campo == 'IT-NO-CREDOR') {
                    $credor[$i]['nome'] = strtoupper(utf8_encode($valor));
                }
                if ($campo == 'IT-CO-UF-CREDOR') {
                    $credor[$i]['uf'] = strtoupper(utf8_encode($valor));
                }
            }

            if ($credor[$i]['tipo_fornecedor'] == 4) {
                $credor[$i]['cpf_cnpj_idgener'] = str_pad(substr($credor[$i]['dado'], 0, 6), 6, "0", STR_PAD_LEFT);
            } else {
                $credor[$i]['cpf_cnpj_idgener'] = $this->formataCnpjCpfTipo($credor[$i]['dado'], $credor[$i]['tipo_fornecedor']);
            }

            $i++;
        }
        gzclose($myfiletxt);

        $this->moverArquivoProcessado($namedestino,$name,$extref);

        $this->moverArquivoProcessado($namedestino,$name,$exttxt);


        return $credor;
    }


}
