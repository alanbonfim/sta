<?php

namespace App\Http\Controllers\Api;

use App\Models\Unidade;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UnidadeController extends BaseController
{
    public function ler()
    {
        $nomearquivo = $this->buscaArquivos();

        $pkCount = (is_array($nomearquivo) ? count($nomearquivo) : 0);
        if ($pkCount != 0) {
            foreach ($nomearquivo as $nome) {
                if (substr($nome['nomearquivo'], 0, 2) == 'ug' or substr($nome['nomearquivo'], 6, 2) == 'ug') {
                    $credor = $this->lerArquivo($nome['nomearquivo'], $nome['case']);

                    foreach ($credor as $e) {
                        $busca = $this->buscaUnidade($e['codigo']);
                        if (!isset($busca->codigo)) {
                            $nova_unidade = new Unidade;
                            $nova_unidade->codigo = $e['codigo'];
                            $nova_unidade->cnpj = $e['cnpj'];
                            $nova_unidade->funcao = $e['funcao'];
                            $nova_unidade->nome = $e['nome'];
                            $nova_unidade->nomeresumido = $e['nomeresumido'];
                            $nova_unidade->uf = $e['uf'];
                            $nova_unidade->orgao = $e['orgao'];
                            $nova_unidade->save();
                        } else {
                            $busca->cnpj = $e['cnpj'];
                            $busca->funcao = $e['funcao'];
                            $busca->nomeresumido = $e['nomeresumido'];
                            $busca->uf = $e['uf'];
                            $busca->orgao = $e['orgao'];
                            $busca->save();
                        }
                    }
                }

            }

            $ok = 'Unidades lidas.';
        } else {
            $ok = 'Não Há arquivos para leitura.';
        }

        return $ok;

    }

    public function lerArquivo($nomeaquivo, $case)
    {
        $path = config('app.path_pendentes');
        $path_processados = config('app.path_processados');
        $name = $path . $nomeaquivo;
        $namedestino = $path_processados . $nomeaquivo;

        if ($case == 0) {
            $extref = ".REF.gz";
            $exttxt = ".TXT.gz";
        }

        if ($case == 1) {
            $extref = ".ref.gz";
            $exttxt = ".txt.gz";
        }

        $myfileref = gzopen($name . $extref, "r") or die("Unable to open file!");

        $i = 0;
        while (!gzeof($myfileref)) {
            $line = gzgets($myfileref);

            if (strlen($line) == 0) break;

            $ref[$i]['column'] = trim(substr($line, 0, 40));
            $ref[$i]['type'] = trim(substr($line, 40, 1));

            if (strstr(trim(substr($line, 42, 4)), ",") != FALSE) {
                $num = explode(",", trim(substr($line, 42, 4)));
                $ref[$i]['size'] = $num[0] + $num[1];
                $ref[$i]['decimal'] = $num[1];
            } else {
                $ref[$i]['size'] = trim(substr($line, 42, 4)) * 1;
                $ref[$i]['decimal'] = 0;
            }

            $ref[$i]['acu'] = ($i == 0) ? $ref[$i]['size'] : $ref[$i]['size'] + $ref[$i - 1]['acu'];
            $i++;
        }
        $NUMCOLS = $i;
        gzclose($myfileref);


        $myfiletxt = gzopen($name . $exttxt, "r") or die("Unable to open file!");
        $i = 0;
        $j = 0;
        while (!gzeof($myfiletxt)) {
            $line = gzgets($myfiletxt);
            for ($j = 0; $j < $NUMCOLS; $j++) {
                $campo = $ref[$j]['column'];
                $inicio = ($j == 0) ? 0 : $ref[$j - 1]['acu'];
                $valor = trim(substr($line, $inicio, $ref[$j]['size']));
                if ($ref[$j]['type'] == "N") {
                    $valor = $valor * pow(10, -$ref[$j]['decimal']);
                }
                if ($campo == 'IT-CO-UNIDADE-GESTORA') {
                    $credor[$i]['codigo'] = str_pad(substr($valor, 0, 6), 6, "0", STR_PAD_LEFT);
                }
                if ($campo == 'IT-NO-UNIDADE-GESTORA') {
                    $credor[$i]['nome'] = strtoupper(utf8_encode($valor));
                }
                if ($campo == 'IT-NO-MNEMONICO-UNIDADE-GESTORA') {
                    $credor[$i]['nomeresumido'] = strtoupper(utf8_encode($valor));
                }
                if ($campo == 'IT-NU-CGC') {
                    $credor[$i]['cnpj'] = str_pad($valor, 14, "0", STR_PAD_LEFT);
                }
                if ($campo == 'IT-CO-UF') {
                    $credor[$i]['uf'] = strtoupper(utf8_encode($valor));
                }
                if ($campo == 'GR-ORGAO') {
                    $credor[$i]['orgao'] = str_pad(substr($valor, 0, 5), 5, "0", STR_PAD_LEFT);
                }
                if ($campo == 'IT-IN-FUNCAO-UG') {
                    $credor[$i]['funcao'] = $valor;
                }
            }

            $i++;
        }

        gzclose($myfiletxt);

        $this->moverArquivoProcessado($namedestino, $name, $extref);

        $this->moverArquivoProcessado($namedestino, $name, $exttxt);

        return $credor;
    }

    public function buscaUnidade($codigo)
    {

        $credor = Unidade::where('codigo', $codigo)
            ->first();

        return $credor;

    }

    public function getUnidadesComEmpenhos($ano, $data = null)
    {
        $ano = $ano ?? date('Y');
        $dataMenosCinco = Carbon::parse($data)->subDay(5);
        $unidades = Unidade::select('unidades.codigo')
            ->distinct()
            ->join('empenhos', 'empenhos.ug', '=', 'unidades.codigo')
            ->where('empenhos.created_at', '>=', $dataMenosCinco)
            ->where('empenhos.numero', 'LIKE', $ano . 'NE%')
            ->where('unidades.funcao', '=', '1')
            ->get();

        return response()->json($unidades, 200);
    }

    public function getUnidadesComRP( $data = null)
    {
        $dataMenosCinco = Carbon::parse($data)->subDay(5);
        $unidades = Unidade::select('unidades.codigo')
            ->distinct()
            ->join('empenhos', 'empenhos.ug', '=', 'unidades.codigo')
            ->where('unidades.funcao', '=', '1')
            ->where('empenhos.rp', true)
            ->where('unidades.funcao', '=', '1')
            ->where('empenhos.created_at', '>=', $dataMenosCinco)
            ->get();

        return response()->json($unidades, 200);
    }
}
