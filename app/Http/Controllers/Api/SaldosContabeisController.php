<?php

namespace App\Http\Controllers\Api;

use App\Models\SaldoContabil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SaldosContabeisController extends BaseController
{
    public function ler()
    {
        $nomearquivo = $this->buscaArquivos();

        $pkCount = (is_array($nomearquivo) ? count($nomearquivo) : 0);
        if ($pkCount != 0) {
            foreach ($nomearquivo as $nome) {
                if (substr($nome['nomearquivo'], 0, 5) == 'saldo' or substr($nome['nomearquivo'], 6, 5) == 'saldo') {
                    $saldocontabil = $this->lerArquivo($nome['nomearquivo'], $nome['case']);

                    foreach ($saldocontabil as $s) {

                        $saldo = new SaldoContabil();
                        $busca = $saldo->buscaSaldoContabil($s['unidade'], $s['gestao'], $s['conta_contabil'],
                            $s['conta_corrente']);

                        if (!isset($busca->unidade)) {
                            $novo_saldo = new SaldoContabil();
                            $novo_saldo->fill($s);
                            $novo_saldo->save();
                        } else {
                            $busca->debito_inicial = $s['debito_inicial'];
                            $busca->credito_inicial = $s['credito_inicial'];
                            $busca->debito_mensal = $s['debito_mensal'];
                            $busca->credito_mensal = $s['credito_mensal'];
                            $busca->saldo = $s['saldo'];
                            $busca->tiposaldo = $s['tiposaldo'];
                            $busca->save();
                        }
                    }
                }

            }

            $ok = 'Saldos Contábeis lidos.';

        } else {
            $ok = 'Não Há arquivos para leitura.';
        }

        return $ok;

    }

    public function retornaSaldoPorUGGestaoContacontabilContacorrenteAno(
        string $ano,
        string $unidade,
        string $gestao,
        string $conta_contabil,
        string $conta_corrente
    ) {
        $retorno = [];

        $busca = new SaldoContabil();
        $retorno = $busca->buscaSaldoContabilAno($ano, $unidade, $gestao, $conta_contabil, $conta_corrente);

        if(isset($retorno)){
            return json_encode($retorno->toArray());
        }else{
            return json_encode($retorno);
        }

    }

    public function retornaSaldoPorUGGestaoContacontabilAno(
        string $ano,
        string $unidade,
        string $gestao,
        string $conta_contabil
    ) {
        $retorno = [];

        $busca = new SaldoContabil();
        $retorno = $busca->buscaSaldoPorUgContaContabilAno($ano,$unidade, $gestao, $conta_contabil);

        return json_encode($retorno->toArray());

    }

    public function retornaSaldoPorUGGestaoAno(
        string $ano,
        string $unidade,
        string $gestao
    ) {
        $retorno = [];

        $busca = new SaldoContabil();
        $retorno = $busca->buscaSaldoPorUgAno($ano, $unidade, $gestao);

        return json_encode($retorno->toArray());

    }

    public function retornaSaldoPorOrgaoAno(
        string $ano,
        string $orgao
    ) {
        $retorno = [];

        $busca = new SaldoContabil();
        $retorno = $busca->buscaSaldoPorOrgaoAno($ano, $orgao);

        return json_encode($retorno->toArray());

    }

    public function lerArquivo(
        $nomeaquivo,
        $case
    ) {
        $path = config('app.path_pendentes');
        $path_processados = config('app.path_processados');
        $name = $path . $nomeaquivo;
        $namedestino = $path_processados . $nomeaquivo;

        if ($case == 0) {
            $extref = ".REF.gz";
            $exttxt = ".TXT.gz";
        }

        if ($case == 1) {
            $extref = ".ref.gz";
            $exttxt = ".txt.gz";
        }

        $myfileref = gzopen($name . $extref, "r") or die("Unable to open file!");

        $i = 0;
        while (!gzeof($myfileref)) {
            $line = gzgets($myfileref);

            if (strlen($line) == 0) {
                break;
            }

            $ref[$i]['column'] = trim(substr($line, 0, 40));
            $ref[$i]['type'] = trim(substr($line, 40, 1));

            if (strstr(trim(substr($line, 42, 4)), ",") != false) {
                $num = explode(",", trim(substr($line, 42, 4)));
                $ref[$i]['size'] = $num[0] + $num[1];
                $ref[$i]['decimal'] = $num[1];
            } else {
                $ref[$i]['size'] = trim(substr($line, 42, 4)) * 1;
                $ref[$i]['decimal'] = 0;
            }

            $ref[$i]['acu'] = ($i == 0) ? $ref[$i]['size'] : $ref[$i]['size'] + $ref[$i - 1]['acu'];
            $i++;
        }
        $NUMCOLS = $i;
        gzclose($myfileref);

        $myfiletxt = gzopen($name . $exttxt, "r") or die("Unable to open file!");
        $i = 0;
        $j = 0;
        while (!gzeof($myfiletxt)) {
            $line = gzgets($myfiletxt);
            for ($j = 0; $j < $NUMCOLS; $j++) {
                $campo = $ref[$j]['column'];
                $inicio = ($j == 0) ? 0 : $ref[$j - 1]['acu'];
                $valor = trim(substr($line, $inicio, $ref[$j]['size']));
                if ($ref[$j]['type'] == "N") {
                    $valor = $valor * pow(10, -$ref[$j]['decimal']);
                }
                if ($campo == 'IT-CO-UNIDADE-GESTORA') {
                    $saldocontabil[$i]['unidade'] = str_pad($valor, 6, "0", STR_PAD_LEFT);
                }

                if ($campo == 'IT-CO-GESTAO') {
                    $saldocontabil[$i]['gestao'] = str_pad($valor, 5, "0", STR_PAD_LEFT);
                }

                if ($campo == 'GR-CODIGO-CONTA') {
                    $saldocontabil[$i]['conta_contabil'] = $valor;
                }

                if ($campo == 'IT-CO-CONTA-CORRENTE-CONTABIL') {
                    $saldocontabil[$i]['conta_corrente'] = $valor;
                }

                if ($campo == 'IT-VA-DEBITO-INICIAL') {
                    $saldocontabil[$i]['debito_inicial'] = number_format($valor, 2, '.', '');
                }

                if ($campo == 'IT-VA-CREDITO-INICIAL') {
                    $saldocontabil[$i]['credito_inicial'] = number_format($valor, 2, '.', '');
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(1)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(2)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(3)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(4)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(5)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(6)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(7)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(8)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(9)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(10)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(11)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(12)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(13)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(14)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(15)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(16)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(17)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(18)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(19)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-DEBITO-MENSAL(20)') {
                    if (!isset($saldocontabil[$i]['debito_mensal'])) {
                        $saldocontabil[$i]['debito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['debito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(1)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(2)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(3)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(4)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(5)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(6)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(7)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(8)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(9)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(10)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(11)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(12)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(13)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(14)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(15)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(16)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(17)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(18)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(19)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }

                if ($campo == 'IT-VA-CREDITO-MENSAL(20)') {
                    if (!isset($saldocontabil[$i]['credito_mensal'])) {
                        $saldocontabil[$i]['credito_mensal'] = number_format($valor, 2, '.', '');
                    } else {
                        $saldocontabil[$i]['credito_mensal'] += number_format($valor, 2, '.', '');
                    }
                }


                if (!isset($saldocontabil[$i]['debito_inicial'])) {
                    $saldocontabil[$i]['debito_inicial'] = 0;
                }

                if (!isset($saldocontabil[$i]['credito_inicial'])) {
                    $saldocontabil[$i]['credito_inicial'] = 0;
                }

                if (!isset($saldocontabil[$i]['debito_mensal'])) {
                    $saldocontabil[$i]['debito_mensal'] = 0;
                }

                if (!isset($saldocontabil[$i]['credito_mensal'])) {
                    $saldocontabil[$i]['credito_mensal'] = 0;
                }


                $saldocontabil[$i]['saldo'] = ($saldocontabil[$i]['debito_inicial'] + $saldocontabil[$i]['debito_mensal']) - ($saldocontabil[$i]['credito_inicial'] + $saldocontabil[$i]['credito_mensal']);

                if ($saldocontabil[$i]['saldo'] < 0) {
                    $saldocontabil[$i]['saldo'] = number_format($saldocontabil[$i]['saldo'] * -1, 2, '.', '');
                    $saldocontabil[$i]['tiposaldo'] = 'C';

                } else {
                    $saldocontabil[$i]['saldo'] = number_format($saldocontabil[$i]['saldo'], 2, '.', '');
                    $saldocontabil[$i]['tiposaldo'] = 'D';
                }

                if ($saldocontabil[$i]['saldo'] == 0) {
                    $saldocontabil[$i]['tiposaldo'] = '';
                }

            }

            $i++;
        }

        gzclose($myfiletxt);

        $this->moverArquivoProcessado($namedestino, $name, $extref);
        $this->moverArquivoProcessado($namedestino, $name, $exttxt);


        return $saldocontabil;
    }

}
