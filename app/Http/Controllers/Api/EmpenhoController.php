<?php

namespace App\Http\Controllers\Api;

use App\Models\Credor;
use App\Models\Empenhodetalhado;
use App\Models\Naturezasubitens;
use App\Models\Obxne;
use App\Models\Ordembancaria;
use App\Models\Planointerno;
use App\Models\Unidade;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Empenho;
use stdClass;


class EmpenhoController extends BaseController
{

    public function ler()
    {
        $nomearquivo = $this->buscaArquivos();

        $pkCount = (is_array($nomearquivo) ? count($nomearquivo) : 0);

        if ($pkCount != 0) {
            foreach ($nomearquivo as $nome) {
                if (substr($nome['nomearquivo'], 0, 2) == 'ne' or substr($nome['nomearquivo'], 6, 2) == 'ne') {
                    $empenho = $this->lerArquivo($nome['nomearquivo'], $nome['case']);


                }
                foreach ($empenho as $e) {
                    $busca = $this->buscaEmpenho($e['ug'], $e['gestao'], $e['numero']);
                    if (!isset($busca->numero)) {
                        $novo_empenho = new Empenho;
                        $novo_empenho->ug = $e['ug'];
                        $novo_empenho->gestao = $e['gestao'];
                        $novo_empenho->numero = $e['numero'];
                        $novo_empenho->numero_ref = $e['numero_ref'];
                        $novo_empenho->emissao = $e['emissao'];
                        $novo_empenho->tipofavorecido = $e['tipofavorecido'];
                        $novo_empenho->favorecido = $e['favorecido'];
                        $novo_empenho->observacao = $e['observacao'];
                        $novo_empenho->fonte = $e['fonte'];
                        $novo_empenho->naturezadespesa = $e['naturezadespesa'];
                        $novo_empenho->planointerno = $e['planointerno'];
                        $novo_empenho->num_lista = $e['num_lista'];
                        $novo_empenho->save();
                    }
                }

            }

            $ok = 'Empenhos lidos.';

        } else {
            $ok = 'Não Há arquivos para leitura.';
        }

        return $ok;

    }

    public function lerArquivo($nomeaquivo, $case)
    {
        $path = config('app.path_pendentes');
        $path_processados = config('app.path_processados');
        $name = $path . $nomeaquivo;
        $namedestino = $path_processados . $nomeaquivo;

        if ($case == 0) {
            $extref = ".REF.gz";
            $exttxt = ".TXT.gz";
        }

        if ($case == 1) {
            $extref = ".ref.gz";
            $exttxt = ".txt.gz";
        }

        $myfileref = gzopen($name . $extref, "r") or die("Unable to open file!");

        $i = 0;
        while (!gzeof($myfileref)) {
            $line = gzgets($myfileref);

            if (strlen($line) == 0) {
                break;
            }

            $ref[$i]['column'] = trim(substr($line, 0, 40));
            $ref[$i]['type'] = trim(substr($line, 40, 1));

            if (strstr(trim(substr($line, 42, 4)), ",") != false) {
                $num = explode(",", trim(substr($line, 42, 4)));
                $ref[$i]['size'] = $num[0] + $num[1];
                $ref[$i]['decimal'] = $num[1];
            } else {
                $ref[$i]['size'] = trim(substr($line, 42, 4)) * 1;
                $ref[$i]['decimal'] = 0;
            }

            $ref[$i]['acu'] = ($i == 0) ? $ref[$i]['size'] : $ref[$i]['size'] + $ref[$i - 1]['acu'];
            $i++;
        }
        $NUMCOLS = $i;
        gzclose($myfileref);

        $myfiletxt = gzopen($name . $exttxt, "r") or die("Unable to open file!");
        $i = 0;
        $j = 0;
        while (!gzeof($myfiletxt)) {
            $line = gzgets($myfiletxt);
            for ($j = 0; $j < $NUMCOLS; $j++) {
                $campo = $ref[$j]['column'];
                $inicio = ($j == 0) ? 0 : $ref[$j - 1]['acu'];
                $valor = trim(substr($line, $inicio, $ref[$j]['size']));
                if ($ref[$j]['type'] == "N") {
                    $valor = $valor * pow(10, -$ref[$j]['decimal']);
                }
                if ($campo == 'GR-UG-GESTAO-AN-NUMERO-NEUQ(1)') {
                    $empenho[$i]['ug'] = substr($valor, 0, 6);
                    $empenho[$i]['gestao'] = substr($valor, 6, 5);
                    $empenho[$i]['numero'] = substr($valor, 11, 12);
                }
                if ($campo == 'GR-AN-NU-DOCUMENTO-REFERENCIA') {
                    $empenho[$i]['numero_ref'] = $valor;
                }
                if ($campo == 'IT-DA-EMISSAO') {
                    $empenho[$i]['emissao'] = substr($valor, 0, 4) . '-' . substr($valor, 4, 2) . '-' . substr($valor,
                            6, 2);
                }
                if ($campo == 'IT-IN-FAVORECIDO') {
                    $empenho[$i]['tipofavorecido'] = $valor;
                }
                if ($campo == 'IT-CO-FAVORECIDO') {
                    if ($empenho[$i]['tipofavorecido'] == 4) {
                        $empenho[$i]['favorecido'] = substr($valor, 0, 6);
                    } else {
                        $empenho[$i]['favorecido'] = $valor;
                    }
                }
                if ($campo == 'IT-TX-OBSERVACAO') {
                    $empenho[$i]['observacao'] = strtoupper(utf8_encode($valor));
                }
                if ($campo == 'GR-FONTE-RECURSO') {
                    $empenho[$i]['fonte'] = $valor;
                }
                if ($campo == 'GR-NATUREZA-DESPESA') {
                    $empenho[$i]['naturezadespesa'] = $valor;
                }
                if ($campo == 'IT-CO-PLANO-INTERNO') {
                    $empenho[$i]['planointerno'] = $valor;
                }
                if ($campo == 'IT-NU-LISTA(1)') {
                    $empenho[$i]['num_lista'] = $valor;
                }
            }

            if ($empenho[$i]['fonte'] == '' and $empenho[$i]['naturezadespesa'] == '0') {
                unset($empenho[$i]);
            }
            $i++;
        }
        gzclose($myfiletxt);

        $this->moverArquivoProcessado($namedestino, $name, $extref);

        $this->moverArquivoProcessado($namedestino, $name, $exttxt);


        return $empenho;
    }

    public function buscaEmpenho($ug, $gestao, $numero)
    {

        $empenho = Empenho::where('ug', $ug)
            ->where('gestao', $gestao)
            ->where('numero', $numero)
            ->first();

        return $empenho;

    }

    public function buscaEmpenhoPorNumero($dado)
    {
        $ug = substr($dado, 0, 6);
        $gestao = substr($dado, 6, 5);
        $anoempenho = strtoupper(substr($dado, 11, 4));
        $numempenho = strtoupper(substr($dado, 11, 12));

        $empenhos = Empenho::
        with(
            array('itens' => function ($query) {

                $sub = Naturezasubitens::select('descricao_subitem')
                    ->where('codigo_nd', DB::raw("empenhos.naturezadespesa"))
                    ->where('codigo_subitem', DB::raw("empenhodetalhado.subitem"));

                $query->select(
                    'empenhodetalhado.numeroli',
                    'empenhodetalhado.numitem',
                    'empenhodetalhado.subitem',
                    'empenhodetalhado.quantidade',
                    'empenhodetalhado.valorunitario',
                    'empenhodetalhado.valortotal',

                    (DB::raw(
                        "(" . $sub->toSql() . ") as subitemdescricao"
                    ))
                )
                    ->join('empenhos', 'empenhos.num_lista', '=', 'empenhodetalhado.numeroli');
            })
        )
        ->select(['empenhos.num_lista', 'empenhos.ug', 'empenhos.gestao', 'empenhos.numero', 'empenhos.emissao'
                , 'empenhos.tipofavorecido as tipocredor'
                , DB::raw('case
                               when empenhos.tipofavorecido = \'1\'
                                   then
                                       substr(empenhos.favorecido, 1, 2) || \'.\' || SUBSTR(empenhos.favorecido, 3, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 6, 3) || \'/\' || SUBSTR(empenhos.favorecido, 9, 4) || \'-\' ||
                                       substr(empenhos.favorecido, 13)
                               when empenhos.tipofavorecido = \'2\'
                                   then
                                       substr(empenhos.favorecido, 1, 3) || \'.\' || substr(empenhos.favorecido, 4, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 7, 3) || \'-\' || substr(empenhos.favorecido, 10)
                               else
                                   empenhos.favorecido
                               end          as cpfcnpjugidgener')
                , DB::raw('case
                               when empenhos.tipofavorecido = \'4\'
                                   then
                                   unidades.nome
                               else
                                   credor.nome
                               end          as nome
                               ')
                , 'empenhos.observacao'
                , 'empenhos.fonte'
                , 'empenhos.naturezadespesa'
                , 'naturezasubitens.descricao_nd as naturezadespesadescricao'
                , 'empenhos.planointerno as picodigo'
                , 'planointerno.descricao as pidescricao'

            ])
            ->distinct()
            ->leftJoin('naturezasubitens', 'naturezasubitens.codigo_nd', '=', 'empenhos.naturezadespesa')
            ->leftJoin('planointerno', 'planointerno.codigo', '=', 'empenhos.planointerno')
            ->leftJoin('unidades', 'unidades.codigo', '=', 'empenhos.favorecido')
            ->leftJoin('credor', DB::raw("replace(replace(replace(credor.cpf_cnpj_idgener, '-', ''), '.', ''), '/', '')"), '=', 'empenhos.favorecido')
            ->where('ug', $ug)
            ->where('gestao', $gestao)
            ->where('numero', $numempenho)
            ->whereNotIn('empenhodetalhado.operacao',['ANULACAO','REFORCO','CANCELAMENTO']);

        return json_encode($empenhos->first());

    }

    public function buscaCredor(string $dado, string $tipo)
    {
        if ($tipo == '4') {
            return $this->buscaCredorUnidade($dado);
        }

        return $this->buscaCredorFavorecido($dado, $tipo);
    }

    public function buscaCredorUnidade(string $ug)
    {
        $unidade = Unidade::where('codigo', $ug)
            ->first();

        if (!isset($unidade->codigo)) {
            $credor['codigo'] = $ug;
            $credor['nome'] = 'CREDOR SEM CADASTRO';

            return $credor;
        }

        $credor['codigo'] = $unidade->codigo;
        $credor['nome'] = $this->trataString($unidade->nome);

        return $credor;
    }

    public function buscaCredorFavorecido(string $dado, string $tipo)
    {
        $dado = $this->formataCnpjCpfTipo($dado, $tipo);

        $favorecido = Credor::where('cpf_cnpj_idgener', $dado)
            ->first();

        if (!isset($favorecido->cpf_cnpj_idgener)) {
            $credor['codigo'] = $dado;
            $credor['nome'] = 'CREDOR SEM CADASTRO';

            return $credor;
        }

        $credor['codigo'] = $favorecido->cpf_cnpj_idgener;
        $credor['nome'] = $this->trataString($favorecido->nome);

        return $credor;
    }

    public function buscaPlanointerno(string $pi)
    {
        $buscapi = Planointerno::where('codigo', $pi)
            ->first();

        if (!isset($buscapi->codigo)) {
            $planointerno['codigo'] = $pi;
            $planointerno['descricao'] = 'PLANO INTERNO NÃO CADASTRADO';

            return $planointerno;
        }

        $planointerno['codigo'] = $buscapi->codigo;
        $planointerno['descricao'] = $this->trataString($buscapi->descricao);

        return $planointerno;
    }

    public function buscaEmpenhoPorDiaUg($ano, $ug, $data = null)
    {

        $dataMenosCinco = Carbon::parse($data)->subDay(5);

        $empenhos = Empenho::
        with(
            array('itens' => function ($query)  {

                $sub = Naturezasubitens::select('descricao_subitem')
                    ->where('codigo_nd', DB::raw("empenhos.naturezadespesa"))
                    ->where('codigo_subitem', DB::raw("empenhodetalhado.subitem") );

                $query->select(
                    'empenhodetalhado.numeroli',
                    'empenhodetalhado.numitem',
                    'empenhodetalhado.subitem',
                    'empenhodetalhado.quantidade',
                    'empenhodetalhado.valorunitario',
                    'empenhodetalhado.valortotal',

                    (DB::raw(
                        "(" . $sub->toSql() . ") as subitemdescricao"
                    ))
                )
                    ->join ('empenhos','empenhos.num_lista','=','empenhodetalhado.numeroli');
            })
        )
            ->select(['empenhos.num_lista', 'empenhos.ug', 'empenhos.gestao', 'empenhos.numero', 'empenhos.emissao'
                , 'empenhos.tipofavorecido as tipocredor'
                , DB::raw('case
                               when empenhos.tipofavorecido = \'1\'
                                   then
                                       substr(empenhos.favorecido, 1, 2) || \'.\' || SUBSTR(empenhos.favorecido, 3, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 6, 3) || \'/\' || SUBSTR(empenhos.favorecido, 9, 4) || \'-\' ||
                                       substr(empenhos.favorecido, 13)
                               when empenhos.tipofavorecido = \'2\'
                                   then
                                       substr(empenhos.favorecido, 1, 3) || \'.\' || substr(empenhos.favorecido, 4, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 7, 3) || \'-\' || substr(empenhos.favorecido, 10)
                               else
                                   empenhos.favorecido
                               end          as cpfcnpjugidgener')
                , DB::raw('case
                               when empenhos.tipofavorecido = \'4\'
                                   then
                                   unidades.nome
                               else
                                   credor.nome
                               end          as nome
                               ')
                , 'empenhos.observacao'
                , 'empenhos.fonte'
                , 'empenhos.naturezadespesa'
                , 'naturezasubitens.descricao_nd as naturezadespesadescricao'
                , 'empenhos.planointerno as picodigo'
                , 'planointerno.descricao as pidescricao'

            ])
            ->distinct()
            ->leftJoin('naturezasubitens', 'naturezasubitens.codigo_nd', '=', 'empenhos.naturezadespesa')
            ->leftJoin('planointerno', 'planointerno.codigo', '=', 'empenhos.planointerno')
            ->leftJoin('unidades', 'unidades.codigo', '=', 'empenhos.favorecido')
            ->leftJoin('credor', DB::raw("replace(replace(replace(credor.cpf_cnpj_idgener, '-', ''), '.', ''), '/', '')"), '=', 'empenhos.favorecido')
            ->where('ug', $ug)
            ->where('empenhos.created_at', '>=', $dataMenosCinco)
            ->where('numero', 'LIKE', $ano . 'NE%')
            ->whereNotIn('empenhodetalhado.operacao',['ANULACAO','REFORCO','CANCELAMENTO']);

        return json_encode($empenhos->get());
    }

    public function buscaNatureza(string $nd)
    {
        $buscand = Naturezasubitens::where('codigo_nd', $nd)
            ->first();

        if (!isset($buscand->codigo_nd)) {
            $naturezadespesa['codigo'] = $nd;
            $naturezadespesa['descricao'] = 'NATUREZA DE DESPESA NÃO CADASTRADA';

            return $naturezadespesa;
        }

        $naturezadespesa['codigo'] = $buscand->codigo_nd;
        $naturezadespesa['descricao'] = $this->trataString($buscand->descricao_nd);

        return $naturezadespesa;
    }

    public function buscaEmpenhoPorAnoUg($ano, $ug)
    {

        $retorno = [];

        $empenhos = Empenho::where('ug', $ug)
            ->where('numero', 'LIKE', $ano . 'NE%')
            ->distinct()
            ->get();

        if (isset($empenhos)) {

            $i = 0;
            foreach ($empenhos as $empenho) {
                $credor = $this->buscaCredor($empenho->favorecido, $empenho->tipofavorecido);
                $ndsubitem = $this->buscaNatureza($empenho->naturezadespesa);
                $planointerno = $this->buscaPlanointerno($empenho->planointerno);

                $empenhodetalhado = new EmpenhodetalhadoController;
                $uggestaoempenho = $empenho->ug . $empenho->gestao . $empenho->numero;

                $retorno[$i]['ug'] = $empenho->ug;
                $retorno[$i]['gestao'] = $empenho->gestao;
                $retorno[$i]['numero'] = $empenho->numero;
                $retorno[$i]['emissao'] = $empenho->emissao;
                $retorno[$i]['tipocredor'] = $empenho->tipofavorecido;
                $retorno[$i]['cpfcnpjugidgener'] = $credor['codigo'];
                $retorno[$i]['nome'] = $credor['nome'];
                $retorno[$i]['observacao'] = $this->trataString($empenho->observacao);
                $retorno[$i]['fonte'] = $empenho->fonte;
                $retorno[$i]['naturezadespesa'] = $ndsubitem['codigo'];
                $retorno[$i]['naturezadespesadescricao'] = $ndsubitem['descricao'];
                $retorno[$i]['picodigo'] = $planointerno['codigo'];
                $retorno[$i]['pidescricao'] = $planointerno['descricao'];
                $retorno[$i]['itens'] = $empenhodetalhado->buscaEmpenhodetalhadoPorNumeroEmpenho($uggestaoempenho, $ndsubitem['codigo']);
                $i++;
            }
        }

        return json_encode($retorno);
    }


}
