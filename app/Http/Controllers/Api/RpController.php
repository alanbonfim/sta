<?php

namespace App\Http\Controllers\Api;

use App\Models\Credor;
use App\Models\Empenhodetalhado;
use App\Models\Naturezasubitens;
use App\Models\Obxne;
use App\Models\Ordembancaria;
use App\Models\Planointerno;
use App\Models\Rp;
use App\Models\Unidade;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Empenho;


class RpController extends BaseController
{
    public function ler()
    {
        $nomearquivo = $this->buscaArquivos();

        $pkCount = (is_array($nomearquivo) ? count($nomearquivo) : 0);

        if ($pkCount != 0) {
            foreach ($nomearquivo as $nome) {
                if (substr($nome['nomearquivo'], 0, 2) == 'rp' or substr($nome['nomearquivo'], 6, 2) == 'rp') {
                    $empenho = $this->lerArquivo($nome['nomearquivo'],$nome['case']);


//                    if (substr($nome['nomearquivo'], 2, 4) == '2020' or substr($nome['nomearquivo'], 8, 4) == '2020') {
//                        $ano =
//                    }else{
//                        $ano = date('Y');
//                    }

                    foreach ($empenho as $e) {
                        $busca = $this->buscaRp($e['ug'], $e['gestao'], $e['numero']);
                        $empenho = $this->buscaEmpenho($e['ug'], $e['gestao'], $e['numero']);
                        if (!isset($busca->numero)) {
                            $novo_empenho = new Rp;
                            $novo_empenho->ug = $e['ug'];
                            $novo_empenho->gestao = $e['gestao'];
                            $novo_empenho->numero = $e['numero'];
                            $novo_empenho->numero_ref = $e['numero_ref'];
                            $novo_empenho->emissao = $e['emissao'];
                            $novo_empenho->tipofavorecido = $e['tipofavorecido'];
                            $novo_empenho->favorecido = $e['favorecido'];
                            $novo_empenho->observacao = $e['observacao'];
                            $novo_empenho->fonte = $e['fonte'];
                            $novo_empenho->naturezadespesa = $e['naturezadespesa'];
                            $novo_empenho->planointerno = $e['planointerno'];
                            $novo_empenho->num_lista = $e['num_lista'];
                            $novo_empenho->save();

                            if (!isset($empenho->numero)) {
                                $empenho->rp = true;
                                $empenho->save();
                            }

                        }else{
                            if (!isset($empenho->numero)) {
                                $empenho->rp = true;
                                $empenho->save();
                            }
                        }
                    }
                }

            }

            $ok = 'RP´s lidos.';

        } else {
            $ok = 'Não Há arquivos para leitura.';
        }

        return $ok;

    }

    protected function buscaRp($ug, $gestao, $numero)
    {

        $rp = Rp::where('ug', $ug)
            ->where('gestao', $gestao)
            ->where('numero', $numero)
            ->first();

        return $rp;

    }

    protected function buscaEmpenho($ug, $gestao, $numero)
    {

        $empenho = Empenho::where('ug', $ug)
            ->where('gestao', $gestao)
            ->where('numero', $numero)
            ->first();

        return $empenho;

    }

    public function lerArquivo($nomeaquivo, $case)
    {
        $path = config('app.path_pendentes');
        $path_processados = config('app.path_processados');
        $name = $path . $nomeaquivo;
        $namedestino = $path_processados . $nomeaquivo;

        if($case == 0){
            $extref = ".REF.gz";
            $exttxt = ".TXT.gz";
        }

        if($case == 1){
            $extref = ".ref.gz";
            $exttxt = ".txt.gz";
        }

        $myfileref = gzopen($name . $extref, "r") or die("Unable to open file!");

        $i = 0;
        while (!gzeof($myfileref)) {
            $line = gzgets($myfileref);

            if (strlen($line) == 0) {
                break;
            }

            $ref[$i]['column'] = trim(substr($line, 0, 40));
            $ref[$i]['type'] = trim(substr($line, 40, 1));

            if (strstr(trim(substr($line, 42, 4)), ",") != false) {
                $num = explode(",", trim(substr($line, 42, 4)));
                $ref[$i]['size'] = $num[0] + $num[1];
                $ref[$i]['decimal'] = $num[1];
            } else {
                $ref[$i]['size'] = trim(substr($line, 42, 4)) * 1;
                $ref[$i]['decimal'] = 0;
            }

            $ref[$i]['acu'] = ($i == 0) ? $ref[$i]['size'] : $ref[$i]['size'] + $ref[$i - 1]['acu'];
            $i++;
        }
        $NUMCOLS = $i;
        gzclose($myfileref);

        $myfiletxt = gzopen($name . $exttxt, "r") or die("Unable to open file!");
        $i = 0;
        $j = 0;
        while (!gzeof($myfiletxt)) {
            $line = gzgets($myfiletxt);
            for ($j = 0; $j < $NUMCOLS; $j++) {
                $campo = $ref[$j]['column'];
                $inicio = ($j == 0) ? 0 : $ref[$j - 1]['acu'];
                $valor = trim(substr($line, $inicio, $ref[$j]['size']));
                if ($ref[$j]['type'] == "N") {
                    $valor = $valor * pow(10, -$ref[$j]['decimal']);
                }
                if ($campo == 'GR-UG-GESTAO-AN-NUMERO-NEUQ(1)') {
                    $empenho[$i]['ug'] = substr($valor, 0, 6);
                    $empenho[$i]['gestao'] = substr($valor, 6, 5);
                    $empenho[$i]['numero'] = substr($valor, 11, 12);
                }
                if ($campo == 'GR-AN-NU-DOCUMENTO-REFERENCIA') {
                    $empenho[$i]['numero_ref'] = $valor;
                }
                if ($campo == 'IT-DA-EMISSAO') {
                    $empenho[$i]['emissao'] = substr($valor, 0, 4) . '-' . substr($valor, 4, 2) . '-' . substr($valor,
                            6, 2);
                }
                if ($campo == 'IT-IN-FAVORECIDO') {
                    $empenho[$i]['tipofavorecido'] = $valor;
                }
                if ($campo == 'IT-CO-FAVORECIDO') {
                    if ($empenho[$i]['tipofavorecido'] == 4) {
                        $empenho[$i]['favorecido'] = substr($valor, 0, 6);
                    } else {
                        $empenho[$i]['favorecido'] = $valor;
                    }
                }
                if ($campo == 'IT-TX-OBSERVACAO') {
                    $empenho[$i]['observacao'] = strtoupper(utf8_encode($valor));
                }
                if ($campo == 'GR-FONTE-RECURSO') {
                    $empenho[$i]['fonte'] = $valor;
                }
                if ($campo == 'GR-NATUREZA-DESPESA') {
                    $empenho[$i]['naturezadespesa'] = $valor;
                }
                if ($campo == 'IT-CO-PLANO-INTERNO') {
                    $empenho[$i]['planointerno'] = $valor;
                }
                if ($campo == 'IT-NU-LISTA(1)') {
                    $empenho[$i]['num_lista'] = $valor;
                }
            }

            if ($empenho[$i]['fonte'] == '' AND $empenho[$i]['naturezadespesa'] == '0') {
                unset($empenho[$i]);
            }
            $i++;
        }
        gzclose($myfiletxt);

        $this->moverArquivoProcessado($namedestino,$name,$extref);

        $this->moverArquivoProcessado($namedestino,$name,$exttxt);


        return $empenho;
    }

    public function buscaRpPorUg($ug)
    {

        $empenhos = Empenho::
        with(
            array('itens' => function ($query)  {

                $sub = Naturezasubitens::select('descricao_subitem')
                    ->where('codigo_nd', DB::raw("empenhos.naturezadespesa"))
                    ->where('codigo_subitem', DB::raw("empenhodetalhado.subitem") );

                $query->select(
                    'empenhodetalhado.numeroli',
                    'empenhodetalhado.numitem',
                    'empenhodetalhado.subitem',
                    'empenhodetalhado.quantidade',
                    'empenhodetalhado.valorunitario',
                    'empenhodetalhado.valortotal',

                    (DB::raw(
                        "(" . $sub->toSql() . ") as subitemdescricao"
                    ))
                )
                    ->join ('empenhos','empenhos.num_lista','=','empenhodetalhado.numeroli');
            })
        )
            ->select(['empenhos.num_lista', 'empenhos.ug', 'empenhos.gestao', 'empenhos.numero', 'empenhos.emissao'
                , 'empenhos.tipofavorecido as tipocredor'
                , DB::raw('case
                               when empenhos.tipofavorecido = \'1\'
                                   then
                                       substr(empenhos.favorecido, 1, 2) || \'.\' || SUBSTR(empenhos.favorecido, 3, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 6, 3) || \'/\' || SUBSTR(empenhos.favorecido, 9, 4) || \'-\' ||
                                       substr(empenhos.favorecido, 13)
                               when empenhos.tipofavorecido = \'2\'
                                   then
                                       substr(empenhos.favorecido, 1, 3) || \'.\' || substr(empenhos.favorecido, 4, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 7, 3) || \'-\' || substr(empenhos.favorecido, 10)
                               else
                                   empenhos.favorecido
                               end          as cpfcnpjugidgener')
                , DB::raw('case
                               when empenhos.tipofavorecido = \'4\'
                                   then
                                   unidades.nome
                               else
                                   credor.nome
                               end          as nome
                               ')
                , 'empenhos.observacao'
                , 'empenhos.fonte'
                , 'empenhos.naturezadespesa'
                , 'naturezasubitens.descricao_nd as naturezadespesadescricao'
                , 'empenhos.planointerno as picodigo'
                , 'planointerno.descricao as pidescricao'

            ])
            ->distinct()
            ->leftJoin('naturezasubitens', 'naturezasubitens.codigo_nd', '=', 'empenhos.naturezadespesa')
            ->leftJoin('planointerno', 'planointerno.codigo', '=', 'empenhos.planointerno')
            ->leftJoin('unidades', 'unidades.codigo', '=', 'empenhos.favorecido')
            ->leftJoin('credor', DB::raw("replace(replace(replace(credor.cpf_cnpj_idgener, '-', ''), '.', ''), '/', '')"), '=', 'empenhos.favorecido')
            ->where('ug', $ug)
            ->where('rp',true)
            ->whereNotIn('empenhodetalhado.operacao',['ANULACAO','REFORCO','CANCELAMENTO'])
            ->get();

        return json_encode($empenhos);

    }
    public function buscaRpPorDiaUg( $ug, $data = null)
    {

        $dataMenosCinco = Carbon::parse($data)->subDay(20);
        $empenhos = Empenho::
        with(
            array('itens' => function ($query)  {

                $sub = Naturezasubitens::select('descricao_subitem')
                    ->where('codigo_nd', DB::raw("empenhos.naturezadespesa"))
                    ->where('codigo_subitem', DB::raw("empenhodetalhado.subitem") );

                $query->select(
                    'empenhodetalhado.numeroli',
                    'empenhodetalhado.numitem',
                    'empenhodetalhado.subitem',
                    'empenhodetalhado.quantidade',
                    'empenhodetalhado.valorunitario',
                    'empenhodetalhado.valortotal',

                    (DB::raw(
                        "(" . $sub->toSql() . ") as subitemdescricao"
                    ))
                )
                    ->join ('empenhos','empenhos.num_lista','=','empenhodetalhado.numeroli');
            })
        )
            ->select(['empenhos.num_lista', 'empenhos.ug', 'empenhos.gestao', 'empenhos.numero', 'empenhos.emissao'
                , 'empenhos.tipofavorecido as tipocredor'
                , DB::raw('case
                               when empenhos.tipofavorecido = \'1\'
                                   then
                                       substr(empenhos.favorecido, 1, 2) || \'.\' || SUBSTR(empenhos.favorecido, 3, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 6, 3) || \'/\' || SUBSTR(empenhos.favorecido, 9, 4) || \'-\' ||
                                       substr(empenhos.favorecido, 13)
                               when empenhos.tipofavorecido = \'2\'
                                   then
                                       substr(empenhos.favorecido, 1, 3) || \'.\' || substr(empenhos.favorecido, 4, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 7, 3) || \'-\' || substr(empenhos.favorecido, 10)
                               else
                                   empenhos.favorecido
                               end          as cpfcnpjugidgener')
                , DB::raw('case
                               when empenhos.tipofavorecido = \'4\'
                                   then
                                   unidades.nome
                               else
                                   credor.nome
                               end          as nome
                               ')
                , 'empenhos.observacao'
                , 'empenhos.fonte'
                , 'empenhos.naturezadespesa'
                , 'naturezasubitens.descricao_nd as naturezadespesadescricao'
                , 'empenhos.planointerno as picodigo'
                , 'planointerno.descricao as pidescricao'

            ])
            ->distinct()
            ->leftJoin('naturezasubitens', 'naturezasubitens.codigo_nd', '=', 'empenhos.naturezadespesa')
            ->leftJoin('planointerno', 'planointerno.codigo', '=', 'empenhos.planointerno')
            ->leftJoin('unidades', 'unidades.codigo', '=', 'empenhos.favorecido')
            ->leftJoin('credor', DB::raw("replace(replace(replace(credor.cpf_cnpj_idgener, '-', ''), '.', ''), '/', '')"), '=', 'empenhos.favorecido')
            ->where('ug', $ug)
            ->where('rp',true)
            ->where('empenhos.created_at', '>=', $dataMenosCinco)
            ->whereNotIn('empenhodetalhado.operacao',['ANULACAO','REFORCO','CANCELAMENTO'])
            ->get();
        return json_encode($empenhos);

    }

}
