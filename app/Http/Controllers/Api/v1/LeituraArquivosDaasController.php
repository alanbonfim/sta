<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Api\EmpenhoController;
use App\Http\Controllers\Api\EmpenhodetalhadoController;
use App\Http\Controllers\Api\OrgaoController;
use App\Http\Controllers\Api\UnidadeController;
use App\Models\Credor;
use App\Models\Empenho;
use App\Models\Empenhodetalhado;
use App\Models\Naturezasubitens;
use App\Models\Orgao;
use App\Models\Planointerno;
use App\Models\SaldoContabil;
use App\Models\Unidade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LeituraArquivosDaasController extends Controller
{
    public function processaDadosArquivoDaas(array $dado, string $tipo_arquivo, string $tipo_extracao, $ug=null)
    {
        $function_name = 'processa' . ucfirst($tipo_extracao) . ucfirst($tipo_arquivo);
        $retorno = $this->$function_name($dado, $ug);

        return $retorno;
    }

    private function processaAllUnidades(array $unidades, $ug = null)
    {
        foreach ($unidades as $unidade) {
            if (isset($unidade['cnpj'])) {
                $uni = new UnidadeController();
                $busca = $uni->buscaUnidade($unidade['CO_UG']);

                if (!isset($busca->codigo)) {
                    $nova_unidade = new Unidade;
                    $nova_unidade->codigo = $unidade['CO_UG'];
                    $nova_unidade->cnpj = $unidade['cnpj'];
                    $nova_unidade->funcao = $unidade['funcao'];
                    $nova_unidade->nome = utf8_encode($unidade['NO_UG']);
                    $nova_unidade->nomeresumido = utf8_encode($unidade['SG_UG']);
                    $nova_unidade->uf = $unidade['ID_UF'];
                    $nova_unidade->orgao = $unidade['CO_ORGAO'];
                    $nova_unidade->save();
                } else {
                    $busca->cnpj = $unidade['cnpj'];
                    $busca->funcao = $unidade['funcao'];
                    $busca->nome = utf8_encode($unidade['NO_UG']);
                    $busca->nomeresumido = utf8_encode($unidade['SG_UG']);
                    $busca->uf = $unidade['ID_UF'];
                    $busca->orgao = $unidade['CO_ORGAO'];
                    $busca->save();
                }
            }
        }
        return 'Unidades Processadas com sucesso!';
    }

    private function processaAllSaldocontabil(array $saldos, $ug = null)
    {
        foreach ($saldos as $saldo) {

            if (isset($saldo['ID_CONTA_CONTABIL'])) {
                $sld = new SaldoContabil();
                $busca = $sld->buscaSaldoContabilAno(
                    $saldo['ano'],
                    str_pad($saldo['CO_UG'], 6, "0", STR_PAD_LEFT),
                    str_pad($saldo['CO_GESTAO'], 5, "0", STR_PAD_LEFT),
                    $saldo['ID_CONTA_CONTABIL'],
                    $saldo['NR_CONTA_CORRENTE']
                );


                if (!isset($busca->unidade)) {
                    $saldocontabil = new SaldoContabil();
                    $saldocontabil->ano = $saldo['ano'];
                    $saldocontabil->unidade = str_pad($saldo['CO_UG'], 6, "0", STR_PAD_LEFT);
                    $saldocontabil->gestao = str_pad($saldo['CO_GESTAO'], 5, "0", STR_PAD_LEFT);
                    $saldocontabil->conta_contabil = $saldo['ID_CONTA_CONTABIL'];
                    $saldocontabil->conta_corrente = $saldo['NR_CONTA_CORRENTE'];
                    $saldocontabil->debito_inicial = number_format($saldo['debito_inicial'], 2, '.', '');
                    $saldocontabil->credito_inicial = number_format($saldo['credito_inicial'], 2, '.', '');
                    $saldocontabil->debito_mensal = number_format($saldo['MOVIMDEVEDORACUMMOEDAORIGEMCO'], 2, '.', '');
                    $saldocontabil->credito_mensal = number_format($saldo['MOVIMCREDORACUMMOEDAORIGEMCON'], 2, '.', '');
                    $saldocontabil->saldo = number_format($saldo['SALDOMOEDAORIGEMCONTACONTBIL'], 2, '.', '');
                    $saldocontabil->tiposaldo = $saldo['tiposaldo'];
                    $saldocontabil->save();
                } else {
                    $busca->debito_inicial = number_format($saldo['debito_inicial'], 2, '.', '');
                    $busca->credito_inicial = number_format($saldo['credito_inicial'], 2, '.', '');
                    $busca->debito_mensal = number_format($saldo['MOVIMDEVEDORACUMMOEDAORIGEMCO'], 2, '.', '');
                    $busca->credito_mensal = number_format($saldo['MOVIMCREDORACUMMOEDAORIGEMCON'], 2, '.', '');
                    $busca->saldo = number_format($saldo['SALDOMOEDAORIGEMCONTACONTBIL'], 2, '.', '');
                    $busca->tiposaldo = $saldo['tiposaldo'];
                    $busca->save();
                }
            }
        }
        return 'Saldos Processados com sucesso!';
    }


    private function processaAllOrgaos(array $orgaos, $ug = null)
    {
        foreach ($orgaos as $orgao) {
            if (isset($orgao['CO_ORGAO'])) {

                $org = new OrgaoController();
                $busca = $org->buscaOrgao($orgao['CO_ORGAO']);

                if (!isset($busca->codigo)) {
                    $novo_orgao = new Orgao;
                    $novo_orgao->orgao_superior = $orgao['orgao_superior'];
                    $novo_orgao->codigo = $orgao['CO_ORGAO'];
                    $novo_orgao->gestao = $orgao['CO_GESTAO'];
                    $novo_orgao->nome = utf8_encode($orgao['NO_ORGAO']);
                    $novo_orgao->save();
                } else {
                    $busca->orgao_superior = $orgao['orgao_superior'];
                    $busca->gestao = $orgao['CO_GESTAO'];
                    $busca->nome = utf8_encode($orgao['NO_ORGAO']);
                    $busca->save();
                }
            }
        }
        return 'Órgãos Processados com sucesso!';
    }

    private function processaAllNaturezasubitem(array $ndsubitens, $ug = null)
    {
        foreach ($ndsubitens as $ndsubitem) {

            if (isset($ndsubitem['NO_NATUREZA_DESPESA'])) {

                $busca = Naturezasubitens::where('codigo_nd', $ndsubitem['CO_NATUREZA_DESPESA'])
                    ->where('codigo_subitem', str_pad($ndsubitem['ID_SUBITEM_NADE'], 2, "0", STR_PAD_LEFT))
                    ->first();

                if (!isset($busca->codigo_nd)) {
                    $nova_ndsubitem = new Naturezasubitens();
                    $nova_ndsubitem->codigo_nd = $ndsubitem['CO_NATUREZA_DESPESA'];
                    $nova_ndsubitem->descricao_nd = utf8_encode($ndsubitem['NO_NATUREZA_DESPESA']);
                    $nova_ndsubitem->codigo_subitem = str_pad($ndsubitem['ID_SUBITEM_NADE'], 2, "0", STR_PAD_LEFT);
                    $nova_ndsubitem->descricao_subitem = utf8_encode($ndsubitem['NO_NATUREZA_DESPESA_DETA']);
                    $nova_ndsubitem->save();
                } else {
                    $busca->descricao_nd = utf8_encode($ndsubitem['NO_NATUREZA_DESPESA']);
                    $busca->descricao_subitem = utf8_encode($ndsubitem['NO_NATUREZA_DESPESA_DETA']);
                    $busca->save();
                }
            }
        }

        return 'Natureza de Despesas Processadas com sucesso!';
    }

    private function processaCargaRestosapagar(array $restosapagar, $ug = null)
    {
        return $this->trataRestosAPagar($restosapagar,$ug);
    }

    private function processaCargaItensempenhos(array $itensempenhos, $ug = null)
    {
        return $this->trataItensEmpenhos($itensempenhos);
    }

    private function processaD1Itensempenhos(array $itensempenhos, $ug = null)
    {
        return $this->trataItensEmpenhos($itensempenhos);
    }

    private function processaD1Empenhos(array $empenhos, $ug = null)
    {
        return $this->trataEmpenhos($empenhos);
    }

    private function processaCargaEmpenhos(array $empenhos, $ug = null)
    {
        return $this->trataEmpenhos($empenhos);
    }

    private function trataItensEmpenhos(array $itensempenhos)
    {

        $ug = '';
        foreach ($itensempenhos as $item) {
            if (isset($item['ID_SUBITEM_NADE'])) {
                $emp = new EmpenhodetalhadoController();
                $busca = $emp->buscaLista(
                    substr($item['ID_LISTA_ITENS_NE'], 0, 6),
                    substr($item['ID_LISTA_ITENS_NE'], 6, 5),
                    $item['ID_LISTA_ITENS_NE'],
                    str_pad($item['ID_ITEM'], 3, "0", STR_PAD_LEFT),
                    str_pad($item['ID_SUBITEM_NADE'], 2, "0", STR_PAD_LEFT)
                );

                if (!isset($busca->ug)) {
                    $itenempenho = new Empenhodetalhado;
                    $itenempenho->ug = substr($item['ID_LISTA_ITENS_NE'], 0, 6);
                    $itenempenho->gestao = substr($item['ID_LISTA_ITENS_NE'], 6, 5);
                    $itenempenho->numeroli = $item['ID_LISTA_ITENS_NE'];
                    $itenempenho->numitem = str_pad($item['ID_ITEM'], 3, "0", STR_PAD_LEFT);
                    $itenempenho->subitem = str_pad($item['ID_SUBITEM_NADE'], 2, "0", STR_PAD_LEFT);
                    $itenempenho->quantidade = number_format($item['QT_ITEM'], 5, ".", '');
                    $itenempenho->valorunitario = number_format($item['VA_ITEM'], 2, ".", '');
                    $itenempenho->valortotal = number_format($item['valortotal'], 2, ".", '');
                    $itenempenho->descricao = strtoupper(utf8_encode($item['TX_OBSERVACAO']));
                    $itenempenho->save();
                }
                $ug = substr($item['ID_LISTA_ITENS_NE'], 0, 6);
            }

        }
        return 'Itens de Empenhos da UG ' . $ug . ' Processados com sucesso!';
    }

    private function trataEmpenhos(array $empenhos)
    {
        $ug = '';
        foreach ($empenhos as $empenho) {
            if (isset($empenho['ID_TP_ENTIDADE'])) {
                switch ($empenho['ID_TP_ENTIDADE']) {
                    case 'PJ':
                        $tipo = "1";
                        break;
                    case 'PF':
                        $tipo = "2";
                        break;
                    case 'IC':
                        $tipo = "3";
                        break;
                    case 'UG':
                        $tipo = "4";
                        break;
                    case 'BC':
                        $tipo = "5";
                        break;
                }

                $this->inserirCredor($empenho['ID_ENTIDADE_FAVO_DOC'], trim($empenho['NO_ENTIDADE']), $tipo);
                $this->inserirPlanoInterno($empenho['ID_PI'], trim($empenho['NO_PI']));

                $dtEmissao = implode('-', array_reverse(explode('/', $empenho['NO_DIA_COMPLETO'])));

                $emp = new EmpenhoController();
                $busca = $emp->buscaEmpenho($empenho['CO_UG'], $empenho['CO_GESTAO'], $empenho['numero']);

                if (!isset($busca->numero)) {
                    $novo_empenho = new Empenho;
                    $novo_empenho->ug = $empenho['CO_UG'];
                    $novo_empenho->gestao = $empenho['CO_GESTAO'];
                    $novo_empenho->numero = $empenho['numero'];
                    $novo_empenho->numero_ref = '';
                    $novo_empenho->emissao = $dtEmissao;
                    $novo_empenho->tipofavorecido = $tipo;
                    $novo_empenho->favorecido = $empenho['ID_ENTIDADE_FAVO_DOC'];
                    $novo_empenho->observacao = strtoupper(utf8_encode($empenho['TX_OBSERVACAO']));
                    $novo_empenho->fonte = $empenho['CO_FONTE_RECURSO_DETA'];
                    $novo_empenho->naturezadespesa = $empenho['CO_NATUREZA_DESPESA'];
                    $novo_empenho->planointerno = $empenho['ID_PI'];
                    $novo_empenho->num_lista = $empenho['ID_LISTA_ITENS_NE'];
                    $novo_empenho->save();
                }
                $ug = $empenho['CO_UG'];
            }
        }

        return 'Empenhos da UG ' . $ug . ' Processados com sucesso!';
    }

    private function trataRestosAPagar(array $rps, string $ug)
    {
        $empenhos_sem_rps = $this->removeRpEmpenhos($ug);

        foreach ($rps as $rp) {
            if (isset($rp['CO_GESTAO'])) {
                $emp = new EmpenhoController();
                $busca = $emp->buscaEmpenho($rp['CO_UG'], $rp['CO_GESTAO'], $rp['NR_CONTA_CORRENTE']);

                if (isset($busca->numero)) {
                    $busca->rp = true;
                    $busca->save();
                }
            }
        }

        return 'Restos a Pagar da UG ' . $ug . ' Processados com sucesso!';
    }

    private function removeRpEmpenhos(string $ug)
    {
        $empenhos = Empenho::where('ug',$ug)
            ->update(['rp' => false]);

        return $empenhos;
    }

    private function inserirCredor(string $cpf_cnpj_idgener, string $nome, string $tipo)
    {
        if ($tipo != 4) {
            $cpf_cnpj_idgener = $this->formataCnpjCpfTipo($cpf_cnpj_idgener, $tipo);

            $busca = Credor::where('cpf_cnpj_idgener', $cpf_cnpj_idgener)
                ->first();

            if (!isset($busca->id)) {
                $credor = new Credor();
                $credor->tipofornecedor = $tipo;
                $credor->cpf_cnpj_idgener = $cpf_cnpj_idgener;
                $credor->nome = utf8_encode($nome);
                $credor->uf = '99';
                $credor->save();
            } else {
                $busca->tipofornecedor = $tipo;
                $busca->nome = utf8_encode($nome);
                $busca->save();
            }

        }
    }

    private function inserirPlanoInterno(string $codigo, string $descricao)
    {

        $busca = Planointerno::where('codigo', $codigo)
            ->first();

        if (!isset($busca->id)) {
            $planointerno = new Planointerno();
            $planointerno->codigo = $codigo;
            $planointerno->descricao = utf8_encode($descricao);
            $planointerno->save();
        } else {
            $busca->descricao = utf8_encode($descricao);
            $busca->save();
        }

    }


}

