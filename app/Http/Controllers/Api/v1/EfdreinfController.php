<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Credor;
use App\Models\Sfpadrao;
use App\Models\Unidade;

class EfdreinfController extends Controller
{

    public function buscarDeducoesPorCompetencia($ug, $gestao, $competencia)
    {
        $retorno = [];

        $codes = config('app.edf_reinf_deducoes');

        $unidade = Unidade::where('codigo', $ug)
//            ->where('gestao', $gestao)
            ->first();

        $docshabil = Sfpadrao::whereHas('deducao', function ($q) use ($codes, $competencia) {
            $q->whereIn('codSit', $codes)->where('txtInscrD',$competencia);
        })
            ->where('codUgEmit', '=', $unidade->codigo)
            ->get();

        foreach ($docshabil as $dochabil){
            $base = new Controller();
            $tipo = $base->getTipoCredor($dochabil->dadosBasicos->codCredorDevedor);
            $cnpj_formatado = $base->formataCnpjCpfTipo($dochabil->dadosBasicos->codCredorDevedor,$tipo);
            $credor = Credor::where('cpf_cnpj_idgener', $cnpj_formatado)->first();
            foreach ($dochabil->deducao as $deducao){
                $retorno[] = [
                    'unidade_gestora' => $unidade->codigo,
                    'cnpj_ug' => $base->formataCnpjCpfTipo($unidade->cnpj,'1'),
                    'cnpj_fornecedor' => $credor->cpf_cnpj_idgener,
                    'nome_fornecedor' => $credor->nome,
                    'serie' => @$deducao->txtInscrB,
                    'numero_documento' => @$deducao->txtInscrC,
                    'data_emissao' => @$deducao->txtInscrD
                ];
            }
        }
        return json_encode($retorno);
    }

}
