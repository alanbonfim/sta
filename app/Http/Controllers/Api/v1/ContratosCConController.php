<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\BaseController;
use App\Jobs\LeituraArquivosDaas;
use App\Jobs\ProcessaCargaEmpenhoDetalhadoDaas;
use App\Jobs\ProcessaCargaEmpenhosDaas;
use App\Jobs\ProcessaCargaNaturezaubitensDaas;
use App\Jobs\ProcessaCargaOrgaosDaas;
use App\Jobs\ProcessaCargaRestosAPagarDaas;
use App\Jobs\ProcessaCargaSaldoContabilDaas;
use App\Jobs\ProcessaCargaUnidadesDaas;
use App\Jobs\ProcessaD1EmpenhoDetalhadoDaas;
use App\Jobs\ProcessaD1EmpenhosDaas;
use App\Jobs\ProcessaD1SaldoContabilDaas;
use App\Models\Orgao;
use App\Models\Unidade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContratosCConController extends Controller
{
    public function buscaUnidades()
    {
        $url = config('app.url_contratos_unidades');

        $busca = new Controller();
        $dados = $busca->buscaDadosUrl($url);

        foreach ($dados as $dado) {
            $unidade = Unidade::where('codigo', $dado->codigo)
                ->first();
            if (isset($unidade->daas)) {
                $unidade->daas = true;
                $unidade->save();
            }
        }

        $retorno = $this->executaBuscaPorUnidade();

        return $retorno;
    }

    private function executaBuscaPorUnidade()
    {
        $base = new BaseController();
        $anos = [
            0 => $base->retornaDataFormatoMaisOuMenosQtdTipo('Y', '-', '5', 'Years', date('Y-m-d')),
            1 => $base->retornaDataFormatoMaisOuMenosQtdTipo('Y', '-', '4', 'Years', date('Y-m-d')),
            2 => $base->retornaDataFormatoMaisOuMenosQtdTipo('Y', '-', '3', 'Years', date('Y-m-d')),
            3 => $base->retornaDataFormatoMaisOuMenosQtdTipo('Y', '-', '2', 'Years', date('Y-m-d')),
            4 => $base->retornaDataFormatoMaisOuMenosQtdTipo('Y', '-', '1', 'Years', date('Y-m-d')),
            5 => $base->retornaDataFormatoMaisOuMenosQtdTipo('Y', '-', '5', 'Days', date('Y-m-d')),
        ];

        $meses = [
            0 => '1,2,3',
            1 => '4,5,6',
            2 => '7,8,9',
            3 => '10,11,12'
        ];

        $unidades = Unidade::where('daas', true)
            ->get();

        ProcessaCargaOrgaosDaas::dispatch()->onQueue('d1Daas');
        ProcessaCargaUnidadesDaas::dispatch()->onQueue('d1Daas');
        ProcessaCargaNaturezaubitensDaas::dispatch()->onQueue('d1Daas');

        foreach ($unidades as $unidade) {
            foreach ($anos as $ano) {
                ProcessaCargaEmpenhosDaas::dispatch($unidade->codigo, $ano)->onQueue('cargaDaas');
                ProcessaCargaRestosAPagarDaas::dispatch($unidade->codigo, $ano)->onQueue('cargaDaas');
                ProcessaCargaEmpenhoDetalhadoDaas::dispatch($unidade->codigo, $ano, $meses[0])->onQueue('cargaDaas');
                ProcessaCargaEmpenhoDetalhadoDaas::dispatch($unidade->codigo, $ano, $meses[1])->onQueue('cargaDaas');
                ProcessaCargaEmpenhoDetalhadoDaas::dispatch($unidade->codigo, $ano, $meses[2])->onQueue('cargaDaas');
                ProcessaCargaEmpenhoDetalhadoDaas::dispatch($unidade->codigo, $ano, $meses[3])->onQueue('cargaDaas');
                ProcessaCargaSaldoContabilDaas::dispatch($unidade->codigo, $ano)->onQueue('cargaDaas');
            }

            ProcessaD1EmpenhosDaas::dispatch($unidade->codigo, $anos[5])->onQueue('d1Daas');
            ProcessaD1EmpenhoDetalhadoDaas::dispatch($unidade->codigo, $anos[5])->onQueue('d1Daas');
            ProcessaD1SaldoContabilDaas::dispatch($unidade->codigo, $anos[5])->onQueue('d1Daas');
        }

        return 'Atualização efetuada.';
    }

    public function executaJdbcClient()
    {
        $path = 'C:\Users\heles.junior\Documents\Projetos\jdbcClient';
        $comando = '"c:\Program Files\Java\jdk-11.0.7\bin\java.exe"  -Dfile.encoding=UTF-8  -classpath C:\Users\heles.junior\Documents\Projetos\jdbcClient\*;jboss-dv-6.3.0-teiid-jdbc.jar -Djavax.net.ssl.trustStore="C:\Users\heles.junior\Documents\Projetos\jdbcClient\daas.serpro.gov.br.jks"  -DdaasUrl="jdbc:teiid:DWTG_Colunar_MP@mms://daas.serpro.gov.br:31000" -DdaasUser="mp_etl_deiop" -DdaasPwd="Brist7qly_pl" -DtodayDate="20210502" -DdaasColumnDelimiter=";"  JdbcClientQueryNaturezasubitem.java';

        shell_exec("cd {$path} && {$comando}");

        return 'Executado';

    }

    public function criaJobsLeituraArquivos()
    {
        $base = new BaseController();
        $nomearquivo = $base->buscaArquivos();
        $pkCount = (is_array($nomearquivo) ? count($nomearquivo) : 0);
        if ($pkCount != 0) {
            foreach ($nomearquivo as $nome) {
                if(substr($nome['nomearquivo'],0,12) == 'restosapagar'){
                    //fila restos a pagar
                    LeituraArquivosDaas::dispatch($nome)->onQueue('leituraArquivosDaasRP');
                }else{
                    LeituraArquivosDaas::dispatch($nome)->onQueue('leituraArquivosDaas');
                }
            }
        }

        return 'Leitura em andamento.';
    }

    public function executaLeituraArquivosViaUrl()
    {
        $base = new BaseController();
        $nomearquivo = $base->buscaArquivos();
        $pkCount = (is_array($nomearquivo) ? count($nomearquivo) : 0);
        if ($pkCount != 0) {
            foreach ($nomearquivo as $nome) {
                if(substr($nome['nomearquivo'],0,12) == 'restosapagar'){
                    //fila restos a pagar
                    $dado_arquivo = $this->lerArquivoDaas($nome);
                }else{
                    $dado_arquivo = $this->lerArquivoDaas($nome);
                }

            }
        }

        return 'Leitura em andamento.';
    }

    public function lerArquivoDaas(array $nome)
    {
        $base = new BaseController();
        $path = config('app.path_pendentes');
        $path_processados = config('app.path_processados');
        $ug = '';

        $nome_arquivo = $path . $nome['nomearquivo'];
        $nome_arquivo_destino = $path_processados . $nome['nomearquivo'];

        $array_nome = explode('_', $nome['nomearquivo']);
        $tipo_arquivo = $array_nome[0];
        if ($array_nome[1] == 'carga' or $array_nome[1] == 'd1') {
            $tipo_extracao = $array_nome[1];
        } else {
            $tipo_extracao = 'all';
        }

        if($tipo_arquivo == 'restosapagar'){
            $ug = $array_nome[2];
        }

        $arquivo = fopen($nome_arquivo.'.txt', 'r');
        $i =0;
        $dado = [];
        while (!feof($arquivo)) {
            $linha = fgets($arquivo, 1024);
            $value = [];
            if($i==0){
                $keys = explode(env('DAAS_DB_DELIMITER1'),$linha);
            }else{
                $value = explode(env('DAAS_DB_DELIMITER1'),$linha);
            }

            if(count($value)) {
                $dados = [];
                foreach($value as $key => $value)
                {
                    $k = trim($keys[$key]);
                    $dados[$k] = trim($value);
                }
                $dado[] = $dados;
            }

            $i++;
        }

        fclose($arquivo);

        if(count($dado)){
            $processa_array_dados = new LeituraArquivosDaasController();
            $retorno = $processa_array_dados->processaDadosArquivoDaas($dado, $tipo_arquivo, $tipo_extracao, $ug);
        }

        $base->moverArquivoProcessado($nome_arquivo_destino,$nome_arquivo,'.txt');

        return $retorno;

    }
}
