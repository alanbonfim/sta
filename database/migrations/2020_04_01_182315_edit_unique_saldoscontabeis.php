<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditUniqueSaldoscontabeis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('saldoscontabeis', function (Blueprint $table) {
            $table->dropUnique([
                'unidade',
                'gestao',
                'conta_contabil',
                'conta_corrente'
            ]);
            $table->unique([
                'ano',
                'unidade',
                'gestao',
                'conta_contabil',
                'conta_corrente'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('saldoscontabeis', function ($table) {
            $table->dropUnique([
                'ano',
                'unidade',
                'gestao',
                'conta_contabil',
                'conta_corrente'
            ]);
            $table->unique([
                'unidade',
                'gestao',
                'conta_contabil',
                'conta_corrente'
            ]);
        });
    }
}
