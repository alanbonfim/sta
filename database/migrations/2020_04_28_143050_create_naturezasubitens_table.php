<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNaturezasubitensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('naturezasubitens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo_nd');
            $table->string('descricao_nd');
            $table->string('codigo_subitem');
            $table->string('descricao_subitem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('naturezasubitens');
    }
}
